**Commands:**

- start - starts server
- stop - stops server
- restart - restarts server
- update - updates server
- autorestart - adds cron job to autorestart server every 2 minutes if it is down (crontab -e to remove it)
- autoupdate - update the server every sunday night with a cron job (crontab -e to remove it)
- check - check if teamspeak is running and if not restart it

***
**YOU ARE RESPONSIBLE FOR ANYTHING THIS SCRIPT DOES TO YOUR TEAMSPEAK, UNLESS IT KILLS YOUR CAT, THEN YOU CAN BLAME WINDOWS**
***