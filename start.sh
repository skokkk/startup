#!/bin/bash

# agree to license if it's not already agreed to
if [[ `ls -a | grep .ts3server_license_accepted` == '' ]]; then
    echo "License hasn't been accepted... We'll accept it for you. Just make sure you read it!"
    touch .ts3server_license_accepted
fi

case "$1" in
start)
    # screwup if not in right directory
    # use the query_ip_whitelist file since that only exists if the server has actually been started for first time
    if [[ `ls | grep query_ip_whitelist` == '' ]]; then
        echo "Wrong directory! Please place this in the server directory!"
        exit
    fi
    ./ts3server_startscript.sh start
    echo "Started teamspeak 3 server."
;;
stop)
    # screwup if not in right directory
    if [[ `ls | grep query_ip_whitelist` == '' ]]; then
        echo "Wrong directory! Please place this in the server directory!"
        exit
    fi

    # stop server
    ./ts3server_startscript.sh stop
    sleep 5
    # make sure it has been killed
    killall -9 ts3server
;;

restart)
    # use teamspeak's built-in script
    ./ts3server_startscript.sh restart
;;
autorestart)
    # screwup if not in right directory
    if [[ `ls | grep query_ip_whitelist` == '' ]]; then
        echo "Wrong directory! Please place this in the server directory!"
        exit
    fi
    # continue
    echo "Installing cron job to check if server is online, and if not restart it."
    CURRENTDIR=`pwd`
    # write out current crontab
    crontab -l > mycron
    # echo new cron into cron file
    echo "*/2 * * * * $CURRENTDIR/./start.sh check >> $CURRENTDIR/restart.log" >> mycron
    # install new cron file
    crontab mycron
    rm mycron
    echo "Cron job installed, if you want to remove it use 'crontab -e'"
    # add something to look for later :)
    echo "remove me and cron job with crontab -e" >> autorestart.here
;;

autoupdate)
    if [[ `ls | grep query_ip_whitelist` == '' ]]; then
        echo "Wrong directory! Please place this in the server directory!"
        exit
    fi
    # continue
    echo "Installing cron job to check if server is online, and if not restart it."
    CURRENTDIR=`pwd`
    # write out current crontab
    crontab -l > mycron
    # echo new cron into cron file
    echo "* 5 11 * * 6 $CURRENTDIR/./start.sh update >> $CURRENTDIR/update.log" >> mycron
    # install new cron file
    crontab mycron
    rm mycron
    echo "Cron job installed, if you want to remove it use 'crontab -e'"
    # add something to look for later :)
    echo "remove me and cron job with crontab -e" >> autorestart.here
;;

serverstart)
    if [[ `ls | grep query_ip_whitelist` == '' ]]; then
        echo "Wrong directory! Please place this in the server directory!"
        exit
    fi
    # continue
    echo "Installing cron job to start server on reboot."
    CURRENTDIR=`pwd`
    # write out current crontab
    crontab -l > mycron
    # echo new cron into cron file
    echo "@reboot $CURRENTDIR/./start.sh check >> $CURRENTDIR/restart.log" >> mycron
    # install new cron file
    crontab mycron
    rm mycron
    echo "Cron job installed, if you want to remove it use 'crontab -e'"
    # add something to look for later :)
    echo "remove me and cron job with crontab -e" >> autorestart.here
;;

check)
    TIME=`date -u`
    # check teamspeak is running
    if pgrep "ts3server" > /dev/null
    then
        echo "Running $TIME"
    else
        echo "Stopped $TIME, restarting"
        CHECKDIR=`find ~/ -name "autorestart.here" | sed s/'autorestart.here'/''/`
        cd $CHECKDIR
        ./ts3server_startscript.sh start
        echo "Started teamspeak 3 server."
    fi
;;

update)
    # warn user
    echo "Please pay attention to this part! Any prompts are essential and you will screwup your server if you put the wrong answer!"
    echo "No one but you will be held responsible for screwing anything up!"

    # stop server
    echo "Stopping Server"
    # stop server if necessary:
    ./ts3server_startscript.sh stop

    # screwup if not in right directory
    if [[ `ls | grep query_ip_whitelist` == '' ]]; then
        echo "Wrong directory! Please place this in the server directory!"
        exit
    fi

    # might extract wrong file
    if [[ ! `ls | grep *.tar.bz2` == '' ]]; then
        echo "WARNING: Please delete/move your archives that contain the .tar.bz2 format:"
        ls | grep *.tar.bz2
    fi

    # backups are important
    read -p "Would you like to backup the current directory? (Y/n): " -n 1 -r
    if [[ ! $REPLY =~ ^[Nn]$ ]]
    then
        mkdir ../backup
        cp -arf * ../backup
        echo "Backup Completed!"
    else
        echo "WARNING: Not backing up directory!"
    fi

    # get latest info from teamspeak's website
    wget https://www.teamspeak.com/downloads
    mv ./downloads ./website.html

    # get user architecture
    ARCH=`uname -m`

    # future use: you can remove the gamed.de
    if [ $ARCH == 'x86_64' ]; then
        ARCH=amd64
        echo "Detected 64 bit operating system."
        URL=`cat website.html | grep teamspeak3-server_linux_amd64 | grep 4players | sed s/'<option value="'/''/ | sed s/">".*/''/ | sed s/'            '/''/ | sed s/'"'/''/`
    else
        ARCH=x86
        echo "Detected 32 bit operating system."
        URL=`cat website.html | grep teamspeak3-server_linux_x86 | grep 4players | sed s/'<option value="'/''/ | sed s/">".*/''/ | sed s/'            '/''/ | sed s/'"'/''/`
    fi

    echo "Downloading: $URL"
    wget $URL
    DLEDURL=`echo $URL | sed -e 's/\(http=1&\).*\(teamspeak3=2&\)/\1\2/'`
    # echo "We have just downloaded $DLEDURL"

    EXTRACT=`ls | grep teamspeak3-server_linux_$ARCH | grep .tar.bz2`

    echo ""
    read -p "Going to extract $EXTRACT (Y/n): " -n 1 -r
    if [[ $REPLY =~ ^[Nn]$ ]]
    then
        echo
        echo "Please extract this archive manually instead."
    else
        bzip2 -dc $EXTRACT | tar xvf -
        echo "Extracted $EXTRACT"
    fi

    # delete archive
    echo "Deleting archive."

    # start update
    echo "Updating files."
    # there will be errors with first one
    mv -rf ./teamspeak3-server_linux_*/* ./

    echo "Cleaning up."
    rm $EXTRACT
    rm -R teamspeak3-server_linux_*

    rm website.html

    echo "Update complete. Starting server."
    ./ts3server_startscript.sh start
;;
*)
echo "$0 (start|stop|restart|update|autorestart|autoupdate|check|serverstart)"
    echo
    echo "start - starts server"
    echo "stop - stops server"
    echo "restart - restarts server"
    echo "update - updates server"
    echo "autorestart - adds cron job to autorestart server (crontab -e to remove it) - basically anti-crash"
    echo "autoupdate - update the server every monday night"
    echo "check - check if teamspeak is running and if not restart it"
    echo "serverstart - start the server automatically on reboot"
exit 1
;;
esac
exit 0
